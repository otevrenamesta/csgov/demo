<?php
/**
 * Site name.
 */
// $prod_base_url = 'https://www.csgov.cz';

/**
 * Domain redirects.
 */
// $aliases = array(
//   'https://csgov.cz' => $prod_base_url,
//   'http://csgov.cz' => $prod_base_url,
//   'http://www.csgov.cz' => $prod_base_url,
//   'http://demo.csgov.otevrenamesta.cz' => 'https://demo.csgov.otevrenamesta.cz',
// );

/**
 * Unshielded base URLs.
 */
$unshieldeds = array(
  // $prod_base_url,
  'https://demo.csgov.otevrenamesta.cz',
  'https://demo2.csgov.otevrenamesta.cz',
);

/**
 * Load services definition file.
 */
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';

/**
 * Host and $base_url.
 */
$protocol = $_SERVER['SERVER_PORT'] == '443' ? 'https://' : 'http://';
$host = $_SERVER['HTTP_HOST'];
$base_url = $protocol . $host;

/**
 * Redirects.
 */
// if (!empty($aliases[$base_url]) && PHP_SAPI !== 'cli') {
//   $domain = $aliases[$base_url];
//   $uri = $_SERVER['REQUEST_URI'];
//   header('HTTP/1.0 301 Moved Permanently');
//   header("Location: $domain$uri");
//   exit();
// }

/**
 * Shield.
 */
$config['shield.settings']['allow_cli'] = TRUE;
if (!in_array($base_url, $unshieldeds)) {
  $config['shield.settings']['shield_enable'] = TRUE;
  $config['shield.settings']['print'] = '';
}

/**
 * Trusted Host Settings.
 */
// $settings['trusted_host_patterns'] = [
//   '^www\.csgov\.cz$',
//   '^csgov\.cz$',
//   '^demo\.csgov\.otevrenamesta\.cz$',
//   '^demo2\.csgov\.otevrenamesta\.cz$',
// ];

// // PROD environment.
// if (DRUPAL_ROOT === '/home/csgov_demo/www/web') {
//   $path = '/var/www/csgov_demo/conf/settings.prod.php';
// }
// // DEV environment.
// elseif (strpos(DRUPAL_ROOT, '/dev.csgov.cz/') !== FALSE) {
//   $path = '/var/www/csgov_demo/conf/settings.dev.php';
// }
//
// // Load settings.
// if (!empty($path) && file_exists($path)) {
//   require($path);
// }

/**
 * Load settings
 * (first suitable env. configuration wins, dev takes precedence over prod)
 */
foreach (array("dev", "prod") as $e) {
  $path = $app_root . '/' . $site_path . "/settings/settings.{$e}.php";

  if (!empty($path) && file_exists($path)) {
    require($path);
    break;
  }
}

/**
 * Config directory.
 */
$settings['config_sync_directory'] = '../config/sync';

/**
 * Allows to remove orphaned files. ??
 */
$config['file.settings']['make_unused_managed_files_temporary'] = TRUE;

if (file_exists($app_root . '/' . $site_path . '/settings/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings/settings.local.php';
}
